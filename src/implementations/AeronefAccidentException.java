package implementations;

public class AeronefAccidentException extends Exception {

	public AeronefAccidentException(String msg){
		super(msg);
	}
}
