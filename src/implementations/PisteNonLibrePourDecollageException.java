package implementations;

public class PisteNonLibrePourDecollageException extends Exception {
	
	public PisteNonLibrePourDecollageException(String msg){
		super(msg);
	}
}
