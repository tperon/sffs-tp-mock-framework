package implementations;

import java.util.ArrayList;
import java.util.List;

import interfaces.IAeronef;
import interfaces.IAvion;
import interfaces.IGouvernail;
import interfaces.IHelicoptere;
import interfaces.IMoteurNucleaire;
import interfaces.IPorteAvions;
import interfaces.IRadar;
import interfaces.ISoute;
import interfaces.ISystemeDeCom;
import interfaces.ISystemeIncendie;

public class PorteAvions implements IPorteAvions {
	private List<IAvion> avionsABord;
	private List<IHelicoptere> helicosABord;
	private ISoute soute;
	private IGouvernail gouvernail;
	private IMoteurNucleaire moteurDroit;
	private IMoteurNucleaire moteurGauche;
	private IRadar radar;
	private ISystemeDeCom sysCom;
	private ISystemeIncendie sysIncendie;
	private boolean etat;
	private boolean pisteLibre;
	private int taillePiste;
	private int capaciteAeronefs;
	
	public PorteAvions(int taille, int capaciteA) {
		this.avionsABord = new ArrayList<IAvion>();
		this.helicosABord = new ArrayList<IHelicoptere>();
		this.etat = true;
		this.pisteLibre = true;
		this.taillePiste = taille;
		this.capaciteAeronefs = capaciteA;
	}
	
	@Override
	public void tourner(int angle, double puissance) {
		try {
			this.gouvernail.setAngle(angle);
		} catch (AngleIncorrect e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.moteurDroit.setPuissance(0);
		this.moteurGauche.setPuissance(puissance);
	}

	@Override
	public void setPuissance(double puissance) {
		this.moteurDroit.setPuissance(puissance);
		this.moteurGauche.setPuissance(puissance);
	}

	@Override
	public void arreter() {
		this.moteurDroit.arreter();
		this.moteurGauche.arreter();
	}

	@Override
	public void reculer() {
		this.moteurDroit.setPuissance(-this.moteurDroit.getPuissance());
		this.moteurGauche.setPuissance(-this.moteurGauche.getPuissance());
	}

	@Override
	public void avancer() {
		try {
			this.gouvernail.setAngle(0);
		} catch (AngleIncorrect e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.moteurDroit.setPuissance(1);
		this.moteurGauche.setPuissance(1);
	}

	@Override
	public boolean getEtat() {
		return etat;
	}

	@Override
	public boolean getAlarmeIncendie() {
		return this.sysIncendie.getAlarm();
	}

	@Override
	public void atterrissage(IAeronef aeronef) throws AeronefAccidentException {
		if(pisteLibre && capaciteAeronefs >= this.getNbAeronefDispo()+1){
			pisteLibre = false;
			int distanceNecessaire = aeronef.atterir();
			if( distanceNecessaire < taillePiste){
				this.dechargerMunition(aeronef);
				if(aeronef instanceof IAvion){
					avionsABord.add((IAvion)aeronef);
					pisteLibre = true;
				} else if (aeronef instanceof IHelicoptere){
					helicosABord.add((IHelicoptere) aeronef);
					pisteLibre = true;
				} else {
					new Exception("OVNI ?");
				}
			} else {
				pisteLibre = true;
				throw new AeronefAccidentException("La longueur de la piste etait trop courte !");
			}
		} else {
			sysIncendie.startAlarm();
			throw new AeronefAccidentException("La piste n'etait pas libre !");
		}
	}

	@Override
	public void chargerMunition(IAeronef aeronef) {
		if(soute.getNbMunition() > 12){
			for(int i=0 ; i<12 ; i++) {
				aeronef.chargerMunition(1);
			}
			this.soute.dechargerMunition(12);
		} 
	}

	@Override
	public void dechargerMunition(IAeronef aeronef) {
		int nbMun = aeronef.dechargerMunition();
		this.soute.chargerMunition(nbMun);
	}

	@Override
	public void decollage(IAeronef aeronef) throws PisteNonLibrePourDecollageException {
		aeronef.chargerMunition(aeronef.getNbMunition() - aeronef.capaciteMunition());
		if(pisteLibre){
			if(aeronef instanceof IAvion){
				avionsABord.remove((IAvion)aeronef);
				pisteLibre = true;
			} else if (aeronef instanceof IHelicoptere){
				helicosABord.remove((IHelicoptere) aeronef);
				pisteLibre = true;
			} else {
				new Exception("Nouvelle technologie ?");
			}
		} else {
			this.dechargerMunition(aeronef);
			throw new PisteNonLibrePourDecollageException("La piste n'est pas libre, retour au garage !");
		}
	}

	@Override
	public List<IAvion> getAvionsDispo() {
		return this.avionsABord;
	}

	@Override
	public List<IHelicoptere> getHelicoptereDispo() {
		return this.helicosABord;
	}

	@Override
	public int getNbAeronefDispo() {
		return this.avionsABord.size() + this.helicosABord.size();
	}

	@Override
	public int getCapaciteAeronef() {
		return capaciteAeronefs;
	}

	@Override
	public int nbObjetsDansCiel() {
		return radar.nbObjetsDansCiel();
	}

	@Override
	public boolean ovniDetecte() {
		return (radar.nbObjetsDansCiel() - radar.getAeronefsAmisDansCiel().size() - radar.getAeronefsEnnemisDansCiel().size()) > 0 ;
	}

	@Override
	public void rappelerLesEnfantsALaMaison() {
		MessageCode msg = new MessageCode();
		msg.setMessage("Il est temps de rentrer pour le gouter !");
		
		List<IAeronef> amis = radar.getAeronefsAmisDansCiel();
		for(int i = 0; i<amis.size(); i++){
			ISystemeDeCom com = amis.get(i).getSystemeDeCom();
			com.envoyerMessage(msg);
		}
	}

	/*GETTERS AND SETTERS : PAS A TESTER */
	public List<IAvion> getAvionsABord() {
		return avionsABord;
	}

	public void setAvionsABord(List<IAvion> avionsABord) {
		this.avionsABord = avionsABord;
	}

	public List<IHelicoptere> getHelicosABord() {
		return helicosABord;
	}

	public void setHelicosABord(List<IHelicoptere> helicosABord) {
		this.helicosABord = helicosABord;
	}

	public ISoute getSoute() {
		return soute;
	}

	public void setSoute(ISoute soute) {
		this.soute = soute;
	}

	public IGouvernail getGouvernail() {
		return gouvernail;
	}

	public void setGouvernail(IGouvernail gouvernail) {
		this.gouvernail = gouvernail;
	}

	public IMoteurNucleaire getMoteurDroit() {
		return moteurDroit;
	}

	public void setMoteurDroit(IMoteurNucleaire moteurDroit) {
		this.moteurDroit = moteurDroit;
	}

	public IMoteurNucleaire getMoteurGauche() {
		return moteurGauche;
	}

	public void setMoteurGauche(IMoteurNucleaire moteurGauche) {
		this.moteurGauche = moteurGauche;
	}

	public IRadar getRadar() {
		return radar;
	}

	public void setRadar(IRadar radar) {
		this.radar = radar;
	}

	public ISystemeDeCom getSysCom() {
		return sysCom;
	}

	public void setSysCom(ISystemeDeCom sysCom) {
		this.sysCom = sysCom;
	}

	public ISystemeIncendie getSysIncendie() {
		return sysIncendie;
	}

	public void setSysIncendie(ISystemeIncendie sysIncendie) {
		this.sysIncendie = sysIncendie;
	}

	public boolean isPisteLibre() {
		return pisteLibre;
	}

	public void setPisteLibre(boolean pisteLibre) {
		this.pisteLibre = pisteLibre;
	}

	public int getTaillePiste() {
		return taillePiste;
	}

	public void setTaillePiste(int taillePiste) {
		this.taillePiste = taillePiste;
	}

	public int getCapaciteAeronefs() {
		return capaciteAeronefs;
	}

	public void setCapaciteAeronefs(int capaciteAeronefs) {
		this.capaciteAeronefs = capaciteAeronefs;
	}

	public void setEtat(boolean etat) {
		this.etat = etat;
	}
}
