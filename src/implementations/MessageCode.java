package implementations;

import interfaces.IMessageCode;

public class MessageCode implements IMessageCode {

	private String message;
	
	@Override
	public void encodeMessage(String key) {
		message += key;
	}

	@Override
	public void decodeMessage(String key) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getMessage() {
		return message;
	}

	@Override
	public void setMessage(String msg) {
		message = msg;
	}

}
