package interfaces;

import java.util.List;

/**
 * 
 * Represente le radar d'un porte-avions de la marine
 * 
 * @author jlanglai, mbergaou, tperon, yalefeuv
 * @version 2013_12_11 - Creation
 */
public interface IRadar {
	public void startDetect();
	public void stopDetect();
	
	/**
	 * Permet de connaitre le nombre d'objets detectes dans le ciel par le radar
	 * @return le nombre d'objet
	 */
	public int nbObjetsDansCiel();
	
	/**
	 * Retourne les aeronefs ennemis detectes dans le ciel
	 * @return la liste des aeronefs ennemis
	 */
	public List<IAeronef> getAeronefsEnnemisDansCiel();
	
	/**
	 * Retourne les aeronefs amis detectes dans le ciel
	 * @return la liste des aeronefs amis
	 */
	public List<IAeronef> getAeronefsAmisDansCiel();
}
