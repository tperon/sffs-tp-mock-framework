package interfaces;

import implementations.AngleIncorrect;

/**
 * 
 * Represente le gouvernail du porte-avions 
 * 
 * @author jlanglai, mbergaou, tperon, yalefeuv
 * @version 2013_12_11 - Creation
 */
public interface IGouvernail {
	public void setAngle(int angle) throws AngleIncorrect;
	public int getAngle();
}
