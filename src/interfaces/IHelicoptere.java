package interfaces;

/**
 * 
 * Represente un helicoptere
 * 
 * @author jlanglai, mbergaou, tperon, yalefeuv
 * @version 2013_12_11 - Creation
 */
public interface IHelicoptere extends IAeronef {

}
