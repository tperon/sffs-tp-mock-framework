package interfaces;

/**
 * 
 * Represente un message code
 * 
 * @author jlanglai, mbergaou, tperon, yalefeuv
 * @version 2013_12_11 - Creation
 */
public interface IMessageCode {
	public void encodeMessage(String key);
	public void decodeMessage(String key);
	public String getMessage();
	public void setMessage(String msg);
}
