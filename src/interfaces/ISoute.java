package interfaces;

import java.util.List;

/**
 * 
 * Represente la soute d'un porte-avions de la marine
 * 
 * @author jlanglai, mbergaou, tperon, yalefeuv
 * @version 2013_12_11 - Creation
 */
public interface ISoute {
	public int getCapaciteMunition();
	public int getNbMunition();
	public void chargerMunition(int nbMunition);
	public void dechargerMunition(int nbMunition);
	public List<IMunition> getMunitions();
}
