package interfaces;

/**
 * 
 * Represente le systeme de communication d'un objet
 * 
 * @author jlanglai, mbergaou, tperon, yalefeuv
 * @version 2013_12_11 - Creation
 */
public interface ISystemeDeCom {
	public void envoyerMessage(IMessageCode msg);
	public IMessageCode recevoirMessage();
}
