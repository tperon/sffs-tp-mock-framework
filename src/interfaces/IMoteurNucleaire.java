package interfaces;

/**
 * 
 * Represente un moteur nucleaire d'un porte-avions de la marine
 * 
 * @author jlanglai, mbergaou, tperon, yalefeuv
 * @version 2013_12_11 - Creation
 */
public interface IMoteurNucleaire {
	public void setPuissance(double puissance);
	public double getPuissance();
	public void arreter();
	public void demarrer();
}
