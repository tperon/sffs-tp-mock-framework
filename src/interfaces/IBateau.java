package interfaces;

/**
 * 
 * Represente un bateau
 * 
 * @author jlanglai, mbergaou, tperon, yalefeuv
 * @version 2013_12_11 - Creation
 */
public interface IBateau {
	public void tourner(int angle,double puissance);
	public void setPuissance(double puissance);
	public void arreter();
	public void reculer();
	public void avancer();
	public boolean getEtat();
	public boolean getAlarmeIncendie();
}
