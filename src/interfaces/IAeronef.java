package interfaces;

/**
 * 
 * Represente un aeronef
 * 
 * @author jlanglai, mbergaou, tperon, yalefeuv
 * @version 2013_12_11 - Creation
 */
public interface IAeronef {
	public int atterir();
	public int decoller();
	public void chargerMunition(int nbMunition);
	public int dechargerMunition();
	public int capaciteMunition();
	public int getNbMunition();
	public ISystemeDeCom getSystemeDeCom();
}
