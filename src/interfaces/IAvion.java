package interfaces;

/**
 * 
 * Represente un avion
 * 
 * @author jlanglai, mbergaou, tperon, yalefeuv
 * @version 2013_12_11 - Creation
 */
public interface IAvion extends IAeronef {

}
