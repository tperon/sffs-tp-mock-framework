package interfaces;

import implementations.AeronefAccidentException;
import implementations.PisteNonLibrePourDecollageException;

import java.util.List;

/**
 * 
 * Represente le porte-avions de la marine
 * 
 * @author jlanglai, mbergaou, tperon, yalefeuv
 * @version 2013_12_11 - Creation
 */

public interface IPorteAvions extends IBateau {
	/**
	 * Methode qui prepare l'atterrissage d'un aeronef sur le porte avion
	 * @param aeronef l'aeronef qui souhaite atterir
	 * @throws AeronefAccidentException 
	 */
	public void atterrissage(IAeronef aeronef) throws AeronefAccidentException;
	
	/**
	 * Charge l'aeronef avec toutes les munitions qu'il peut porter
	 * @param aeronef l'aeronef a charger
	 */
	public void chargerMunition(IAeronef aeronef);
	
	/**
	 * Decharge l'aeronef avec toutes les munitions qu'il possede
	 * @param aeronef l'aeronef a decharger
	 */
	public void dechargerMunition(IAeronef aeronef);
	
	/**
	 * preparation du porte-avions pour le decollage
	 * @param aeronef l'aeronef qui veut decoller
	 * @throws PisteNonLibrePourDecollageException 
	 */
	public void decollage(IAeronef aeronef) throws PisteNonLibrePourDecollageException;
	
	/**
	 * Permet de savoir les avions qui sont presents sur le porte-avions
	 * @return la liste des avions presents
	 */
	public List<IAvion> getAvionsDispo();
	
	/**
	 * Permet de savoir les helicopteres qui sont presents sur le porte-avions
	 * @return la liste des helicopteres presents
	 */
	public List<IHelicoptere> getHelicoptereDispo();
	
	/**
	 * Retourne le nombre d'aeronefs presents sur le porte-avions
	 * @return le nombre d'aeronefs
	 */
	public int getNbAeronefDispo();
	
	/**
	 * Retourne la capacite en terme d'aeronef du porte-avions
	 * @return la capacite du porte-avions
	 */
	public int getCapaciteAeronef();
	
	/**
	 * Permet de connaitre le nombre d'objets detectes dans le ciel par le radar
	 * @return le nombre d'objet
	 */
	public int nbObjetsDansCiel();
	
	/**
	 * Permet de savoir si on detecte des objets volants non identifies
	 * @return true si on detecte des ovni, false sinon
	 */
	public boolean ovniDetecte();
	
	/**
	 * Rappelle tous les avions amis à bord 
	 */
	public void rappelerLesEnfantsALaMaison();
}
