package interfaces;

/**
 * 
 * Represente le systeme d'incendie d'un bateau
 * 
 * @author jlanglai, mbergaou, tperon, yalefeuv
 * @version 2013_12_11 - Creation
 */
public interface ISystemeIncendie {
	public boolean getAlarm();
	public void startAlarm();
	public void stopAlarm();
}
